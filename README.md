<h1 style="text-align: center;"><span style="color: #3366ff;"><strong>-= MirrorCopy =-</strong></span></h1>

<a href=""><img src="https://gitlab.com/testertv/mirrorcopy/-/raw/master/img.jpg?raw=true" alt="test-pattern-152459-1280" border="0"></a>

MirrorCopy is designed for the synchronization of two folders. The following options are possible:

- Synchronize two folders of your choice. Everything that is changed in the main folder is also changed in the destination folder (delete, copy, cut, create,...).

- Synchronize two folders of your choice. All files created or modified in the main folder are synchronized with the destination folder. If something is deleted in the main folder, it will still be kept in the destination folder (backup function).

- Simple copy between two folders. This is faster than usual because the program bypasses the Windows graphical interface.
